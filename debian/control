Source: libfyaml
Priority: optional
Maintainer: Jose Luis Blanco Claraco <joseluisblancoc@gmail.com>
Uploaders: Timo Röhling <roehling@debian.org>
Build-Depends: debhelper-compat (= 13),
               pkg-config,
               libltdl-dev
Standards-Version: 4.6.2
Section: libs
Homepage: https://github.com/pantoniou/libfyaml
Vcs-Git: https://salsa.debian.org/jlblancoc/libfyaml-gbp.git
Vcs-Browser: https://salsa.debian.org/jlblancoc/libfyaml-gbp

Package: libfyaml-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libfyaml0 (= ${binary:Version})
Description: Fully feature complete YAML parser and emitter (headers)
 Fully feature complete YAML parser and emitter, supporting the latest YAML
 spec and passing the full YAML testsuite.
 .
 It is designed to be very efficient, avoiding copies of data, and has no
 artificial limits like the 1024 character limit for implicit keys.
 .
 This package contains development headers and static libraries.

Package: libfyaml-utils
Section: devel
Architecture: any
Depends: ${misc:Depends}, ${shlibs:Depends}, libfyaml0 (= ${binary:Version})
Description: Fully feature complete YAML parser and emitter (CLI tools)
 Fully feature complete YAML parser and emitter, supporting the latest YAML
 spec and passing the full YAML testsuite.
 .
 It is designed to be very efficient, avoiding copies of data, and has no
 artificial limits like the 1024 character limit for implicit keys.
 .
 This package contains utilities manipulating YAML files.

Package: libfyaml0
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${shlibs:Depends}, ${misc:Depends}
Description: Fully feature complete YAML parser and emitter (library)
 Fully feature complete YAML parser and emitter, supporting the latest YAML
 spec and passing the full YAML testsuite.
 .
 It is designed to be very efficient, avoiding copies of data, and has no
 artificial limits like the 1024 character limit for implicit keys.
